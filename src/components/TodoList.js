import React from "react";
import TodoInput from "./TodoInput";
import Todo from "./Todo";
import { useSelector, useDispatch } from "react-redux";
import { addTodo, updateTodo } from "../redux/action";

const TodoList = () => {
  const state = useSelector((state) => ({ ...state.todos }));
  let dispatch = useDispatch();

  const create = (newTodo) => {
    dispatch(addTodo(newTodo));
  };

  const update = (id, updatedStatus) => {
    dispatch(updateTodo({ id, updatedStatus }));
  };

  return (
    <div class="container">
      <div>
        <h1 style={{ color: "red" }}>React Redux Todo App</h1>
        <TodoInput createTodo={create} />
        {state.todos.length > 0 &&
          state.todos.map((todo) => (
            <div key={todo.id} class="card m-3">
              <div class="p-3">
                <h3 style={{ color: "#229954", fontWeight: "bold" }}>
                  Title: {todo.title}
                </h3>
                <h5>Description: {todo.description}</h5>
                <h5>Status: {todo.status}</h5>
                <Todo id={todo.id} status={todo.status} updateTodo={update} />
              </div>
            </div>
          ))}
      </div>
    </div>
  );
};

export default TodoList;
