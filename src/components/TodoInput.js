import React, { useState } from "react";

const TodoInput = ({ createTodo }) => {
  const [title, setTitle] = useState("");
  const [description, setDescription] = useState("");
  const [status, setStatus] = useState("");

  const handleSubmit = (e) => {
    e.preventDefault();
    const todo = {
      title: title,
      description: description,
      status: status,
    };
    console.log(todo);
    createTodo(todo);
    setTitle("");
    setDescription("");
    setStatus("");
  };
  return (
    <div class="container">
      <form class="jumbotron" onSubmit={handleSubmit}>
        <div class="form-group">
          <input
            type="text"
            class="form-control"
            style={{ marginBottom: 20 }}
            placeholder="Enter Title"
            id="title"
            name="title"
            value={title}
            onChange={(e) => setTitle(e.target.value)}
          />
        </div>
        <div class="form-group">
          <input
            type="text"
            class="form-control"
            style={{ marginBottom: 20 }}
            placeholder="Enter Description"
            id="description"
            name="description"
            value={description}
            onChange={(e) => setDescription(e.target.value)}
          />
        </div>

        <div class="form-group">
          <select
            type="text"
            className="form-control"
            id="status"
            name="status"
            value={status}
            onChange={(e) => setStatus(e.target.value)}
            style={{ marginBottom: 20 }}
          >
            <option value="Select" hidden>
              Select Status
            </option>
            <option value="todo" style={{ color: "#000" }}>
              todo
            </option>
            <option value="in-progress" style={{ color: "#000" }}>
              in-progress
            </option>
            <option value="completed" style={{ color: "#000" }}>
              completed
            </option>
          </select>
        </div>

        <button class="btn btn-primary"> Add Todo</button>
      </form>
    </div>
  );
};

export default TodoInput;
