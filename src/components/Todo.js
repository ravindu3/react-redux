import React, { useState } from "react";

const Todo = ({ status, id, updateTodo }) => {
  const [isEditing, setIsEditing] = useState(false);
  const [editStatus, setEditStatus] = useState(status);

  const handleUpdate = (e) => {
    e.preventDefault();
    console.log(editStatus);
    updateTodo(id, editStatus);
    setIsEditing(false);
  };

  return (
    <div>
      {isEditing ? (
        <form onSubmit={handleUpdate}>
          <select
            type="text"
            name="status"
            value={editStatus}
            onChange={(e) => setEditStatus(e.target.value)}
            style={{ marginBottom: 20, width: 200, height: 30 }}
          >
            <option value="Select" hidden>
              Select Status
            </option>
            <option value="todo" style={{ color: "#000" }}>
              todo
            </option>
            <option value="in-progress" style={{ color: "#000" }}>
              in-progress
            </option>
            <option value="completed" style={{ color: "#000" }}>
              completed
            </option>
          </select>
          <button class="btn m-1" style={{ background: "green" }}>
            Save
          </button>
        </form>
      ) : null}

      <div>
        <button
          onClick={() => setIsEditing(true)}
          style={{
            color: "#000",
            background: "yellow",
            fontWeight: "bold",
            marginLeft: "80%",
          }}
          class="btn"
        >
          Update
        </button>
      </div>
    </div>
  );
};

export default Todo;
