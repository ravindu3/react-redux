import * as types from "./actionTypes";
import { v4 as uuidv4 } from "uuid";

const initialState = {
  todos: [
    {
      id: 1,
      title: "AF",
      description: "Project",
      status: "todo",
      completed: false,
    },
  ],
};

const todosReducer = (state = initialState, action) => {
  switch (action.type) {
    case types.ADD_TODO:
      const newTodo = {
        id: uuidv4(),
        title: action.payload.title,
        description: action.payload.description,
        status: action.payload.status,
        completed: false,
      };

      const addedTodos = [...state.todos, newTodo];
      console.log("added todo is:" + addedTodos);
      return {
        ...state,
        todos: addedTodos,
      };

    case types.UPDATE_TODO:
      const updatedTodos = state.todos.map((todo) => {
        console.log("payload id:" + action.payload.id);
        if (todo.id === action.payload.id) {
          console.log("payload id:" + action.payload.id);
          return { ...todo, status: action.payload.updatedStatus };
        }
        return todo;
      });
      return {
        ...state,
        todos: updatedTodos,
      };

    case types.COMPLETE_TODO:
      const toggleTodos = state.todos.map((t) =>
        t.id === action.payload.id
          ? { ...action.payload, completed: !action.payload.completed }
          : t
      );
      return {
        ...state,
        todos: toggleTodos,
      };
    default:
      return state;
  }
};

export default todosReducer;
